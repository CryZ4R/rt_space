class ScheduleItem {
  String title;
  String cabinet;
  String lecturer;
  String place;
  String time;

  ScheduleItem({this.cabinet, this.title, this.lecturer, this.place, this.time});
}