import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:new_space/screens/home/home.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;

void main() {
  buildDatabase();
  firstUpdateDb();
  runApp(MyApp());
}

void buildDatabase() async {
  WidgetsFlutterBinding.ensureInitialized();
  var db = await openDatabase('my_space.db');
//  await db.execute('DROP TABLE IF EXISTS schedule');
  await db.execute(
      'CREATE TABLE IF NOT EXISTS schedule(id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR, type VARCHAR, day INTEGER, week INTEGER, place VARCHAR, cabinet VARCHAR, time VARCHAR)');
  await db.execute(
      'CREATE TABLE IF NOT EXISTS system_data(id INTEGER PRIMARY KEY AUTOINCREMENT, last_update DATETIME)');
}

void firstUpdateDb() async {
  var urlAll = 'http://95.165.143.79:8000/api/schedule';
  var urlUpdate = 'http://95.165.143.79:8000/api/update';

  var db = await openDatabase('my_space.db');
  var data = await db.rawQuery('SELECT * FROM schedule');
  if (data.length == 0) {
    var response = await http.get(urlAll);
    var jsonData = jsonDecode(response.body);
    for (var item in jsonData) {
      await db.execute(
          'INSERT OR IGNORE INTO schedule (title, type, day, week, place, cabinet, time) VALUES(?, ?, ?, ?, ?, ?, ?)',
          [
            item['title'],
            item['type'],
            item['day'],
            item['week'],
            item['place'],
            item['cabinet'],
            item['time'],
          ]);
      await db.execute('INSERT INTO system_data (last_update) VALUES(?)', [
        DateTime.now().toIso8601String(),
      ]);
    }
  } else {
    var date =
        await db.rawQuery('SELECT * FROM system_data ORDER BY id DESC LIMIT 1');
    if (date.length > 0) {
      var last_update = DateTime.parse(date[0]['last_update']);
      var response = await http.get(urlUpdate);
      var json = jsonDecode(response.body);
      try {
        var server_update = DateTime.parse(json['last_update']);
        if (server_update.isAfter(last_update)) {
          await db.execute('DELETE FROM schedule');
          firstUpdateDb();
        }
      } catch (e) {}
    } else {
      await db.execute('DELETE FROM schedule');
      firstUpdateDb();
    }
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
