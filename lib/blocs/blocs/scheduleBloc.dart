import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:new_space/blocs/events/scheduleEvents.dart';
import 'package:new_space/blocs/states/scheduleStates.dart';
import 'package:sqflite/sqflite.dart';

class ScheduleListBloc extends Bloc<ScheduleEvent, ScheduleState> {
  List scheduleItems;

  @override
  ScheduleState get initialState => ScheduleLoading();

  @override
  Stream<ScheduleState> mapEventToState(ScheduleEvent event) async* {
    if (event is ScheduleLoad) {
      yield ScheduleLoading();
      try {
        final List items = await loadScheduleItems(event);
        yield ScheduleLoaded(
            selectedIndex: event.props[1], scheduleItems: items);
      } catch (e) {
        yield ScheduleLoaded(selectedIndex: event.props[1]);
      }
    } else {
      yield ScheduleLoaded();
    }
  }

  Future<List> loadScheduleItems(ScheduleEvent event) async {
    var db = await openDatabase('my_space.db');
    final items =
        await db.rawQuery('SELECT * FROM schedule WHERE day=? AND week=? ORDER BY time', [
      DateTime.parse(event.props[0].toString()).weekday,
      (event.props[2]) ? 1 : 0,
    ]);
    return items;
  }
}
