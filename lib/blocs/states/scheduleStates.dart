import 'package:equatable/equatable.dart';

abstract class ScheduleState extends Equatable {
  const ScheduleState();

  @override
  List get props => [];
}


class ScheduleLoading extends ScheduleState {}

class ScheduleLoaded extends ScheduleState {
  final int selectedIndex;
  final List scheduleItems;

  ScheduleLoaded({this.selectedIndex, this.scheduleItems});

  @override
  List<Object> get props => [selectedIndex];
}