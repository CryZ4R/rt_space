import 'package:equatable/equatable.dart';

abstract class ScheduleEvent extends Equatable {
  const ScheduleEvent();

  @override
  List<Object> get props => [];
}

class ScheduleLoad extends ScheduleEvent {
  final DateTime date;
  final int selectedIndex;
  final bool isNumerator;

  ScheduleLoad({this.date, this.selectedIndex, this.isNumerator});

  @override
  List<Object> get props => [date, selectedIndex, isNumerator];

  @override
  String toString() => 'load schedule {date: $date, index: $selectedIndex, isNumerator: $isNumerator}';
}