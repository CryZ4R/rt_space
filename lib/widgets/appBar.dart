import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:new_space/blocs/blocs/scheduleBloc.dart';
import 'package:new_space/blocs/events/scheduleEvents.dart';

class SpaceAppBar extends StatelessWidget with PreferredSizeWidget {
  final AppBar appBar;
  final bool showCalendarTop;

  SpaceAppBar({this.appBar, this.showCalendarTop});

  @override
  Widget build(BuildContext context) {
    final List<Widget> widgets = [];

    if (showCalendarTop) {
      widgets.add(IconButton(
        icon: Icon(
          Icons.calendar_today,
          color: Colors.white,
        ),
        onPressed: () {
          DatePicker.showDatePicker(context,
              showTitleActions: true,
              minTime: DateTime.now(),
              maxTime: DateTime(2020, 5, 31),
              onChanged: (date) {}, onConfirm: (date) {
            var selectedIndex = -1;
            if (date
                    .difference(DateTime(DateTime.now().year,
                        DateTime.now().month, DateTime.now().day))
                    .inDays <
                14) {
              selectedIndex = date
                  .difference(DateTime(DateTime.now().year,
                      DateTime.now().month, DateTime.now().day))
                  .inDays;
            }

            BlocProvider.of<ScheduleListBloc>(context).add(ScheduleLoad(
                date: date, selectedIndex: selectedIndex, isNumerator: false));
          }, currentTime: DateTime.now(), locale: LocaleType.ru);
        },
      ));
    }

    return AppBar(
      title: Text('My Space'),
      backgroundColor: Colors.deepPurple,
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
