import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:new_space/utils/humanize_values.dart';
import 'package:new_space/blocs/blocs/scheduleBloc.dart';
import 'package:new_space/blocs/events/scheduleEvents.dart';

class CalendarTop extends StatelessWidget {
  List week = getWeek();
  int selectedIndex = 0;

  CalendarTop({this.selectedIndex});

  static List getWeek() {
    List weekDays = [];
    for (int i = 0; i < 14; i++) {
      weekDays.add(DateTime.now().add(Duration(days: i)));
    }
    return weekDays;
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListView.builder(
          physics:
              BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          scrollDirection: Axis.horizontal,
          itemCount: 14,
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 4),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      bool isNumerator = false;
                      int startWeek = 6;
                      int dayOfYear =
                          int.parse(DateFormat("D").format(week[index]));
                      int currentWeek =
                          ((dayOfYear - week[index].weekday + 10) / 7).floor();

                      if ((currentWeek - startWeek) % 2  == 0) {
                        isNumerator = true;
                      }

                      BlocProvider.of<ScheduleListBloc>(context).add(
                          ScheduleLoad(
                              date: week[index], selectedIndex: index, isNumerator: isNumerator));
                    },
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 50,
                            height: 20,
                            child: Center(
                              child: Text(
                                Humanize.dayOfWeekFromDateTime(week[index]),
                              ),
                            ),
                          ),
                          Container(
                            width: 50,
                            height: 30,
                            color: (index == selectedIndex ||
                                    (index == 0 && selectedIndex == null))
                                ? Color.fromRGBO(64, 75, 96, .8)
                                : Color.fromRGBO(64, 75, 96, .4),
                            child: Center(
                              child: Text(
                                week[index].day.toString(),
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
