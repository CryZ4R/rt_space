import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScheduleItemCard extends StatefulWidget {
  var itemData;

  ScheduleItemCard({this.itemData});

  @override
  State<StatefulWidget> createState() => ScheduleItemCardState();
}

class ScheduleItemCardState extends State<ScheduleItemCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
      ),
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9), borderRadius: BorderRadius.circular(20),),
        child: ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0, top: 15, bottom: 15),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child: Text(
              "${widget.itemData['time']}",
              style: TextStyle(color: Colors.white),
            ),
          ),
          title: Text(
            "${widget.itemData['title']} - ${widget.itemData['type']}",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          subtitle: Row(
            children: <Widget>[
              Text(
                  "${widget.itemData['place']} - ${widget.itemData['cabinet']}",
                  style: TextStyle(color: Colors.white))
            ],
          ),
        ),
      ),
    );
  }
}
