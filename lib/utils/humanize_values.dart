class Humanize {
  static String dateTime(DateTime dateTime) {
    return '${dateTime.month}/${dateTime.day}/${dateTime.year}';
  }

  static String enumValue(String enumVal) {
    return enumVal.split('.').last;
  }

  static String dayOfWeekFromDateTime(DateTime dateTime) {
    switch (dateTime.weekday) {
      case 1:
        return 'Пн';
      case 2:
        return 'Вт';
      case 3:
        return 'Ср';
      case 4:
        return 'Чт';
      case 5:
        return 'Пт';
      case 6:
        return 'Сб';
      case 7:
      default:
        return 'Вс';
    }
  }

  static String listOfDays(List<String> days) {
    var stringList = days.fold("", (String prev, String ele) {
      return "$prev ${enumValue(ele)},";
    });

    return stringList.substring(0, stringList.length - 1);
  }
}
