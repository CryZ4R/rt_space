import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:new_space/blocs/blocs/scheduleBloc.dart';
import 'package:new_space/blocs/events/scheduleEvents.dart';
import 'package:new_space/blocs/states/scheduleStates.dart';
import 'package:new_space/widgets/appBar.dart';
import 'package:new_space/widgets/calendarTop.dart';
import 'package:new_space/widgets/scheduleItemCard.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  var bottomSelectedIndex = 0;
  var showCalendarTop = true;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  void pageChanged(int index) {
    setState(() {
      if (index != 0) {
        showCalendarTop = false;
      } else {
        showCalendarTop = true;
      }

      pageController.jumpToPage(index);

      bottomSelectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isNumerator = false;
    int startWeek = 6;
    int dayOfYear = int.parse(DateFormat("D").format(DateTime.now()));
    int currentWeek = ((dayOfYear - DateTime.now().weekday + 10) / 7).floor();

    if ((currentWeek - startWeek) % 2 == 0) {
      isNumerator = true;
    }

    Widget buildSchedule() {
      return BlocBuilder<ScheduleListBloc, ScheduleState>(
        builder: (context, state) {
          return Column(
            children: <Widget>[
              CalendarTop(
                selectedIndex:
                    (state is ScheduleLoaded) ? state.selectedIndex : 0,
              ),
              (state is ScheduleLoaded)
                  ? (state.scheduleItems.isNotEmpty)
                      ? Flexible(
                          flex: 6,
                          child: ListView.builder(
                              physics: BouncingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              itemCount: state.scheduleItems.length,
                              itemBuilder: (context, index) {
                                return ScheduleItemCard(
                                    itemData: state.scheduleItems[index]);
                              }),
                        )
                      : Flexible(
                          flex: 6,
                          child: Center(
                            child: Text('На этот день ничего нет'),
                          ),
                        )
                  : Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
            ],
          );
        },
      );
    }

    Widget buildPageView() {
      return PageView(
        controller: pageController,
        onPageChanged: pageChanged,
        children: <Widget>[
          buildSchedule(),
          Container(
            child: Center(
              child: Text('Информации о констультацих нет'),
            ),
          ),
          Container(
            child: Center(
              child: Text('Свежих новостей нет'),
            ),
          ),
        ],
      );
    }

    return BlocProvider<ScheduleListBloc>(
      create: (context) => ScheduleListBloc()
        ..add(ScheduleLoad(
            selectedIndex: 0, date: DateTime.now(), isNumerator: isNumerator)),
      child: Scaffold(
        appBar: SpaceAppBar(
          appBar: AppBar(),
          showCalendarTop: showCalendarTop,
        ),
        body: buildPageView(),
        bottomNavigationBar: BubbleBottomBar(
          opacity: .2,
          currentIndex: bottomSelectedIndex,
          onTap: pageChanged,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
          elevation: 8,
          hasNotch: true,
          hasInk: true,
          inkColor: Colors.black12,
          items: <BubbleBottomBarItem>[
            BubbleBottomBarItem(
                backgroundColor: Colors.red,
                icon: Icon(
                  Icons.dashboard,
                  color: Colors.black,
                ),
                activeIcon: Icon(
                  Icons.dashboard,
                  color: Colors.red,
                ),
                title: Text("Расписание")),
            BubbleBottomBarItem(
                backgroundColor: Colors.deepPurple,
                icon: Icon(
                  Icons.access_time,
                  color: Colors.black,
                ),
                activeIcon: Icon(
                  Icons.access_time,
                  color: Colors.deepPurple,
                ),
                title: Text("Экзамены")),
            BubbleBottomBarItem(
                backgroundColor: Colors.indigo,
                icon: Icon(
                  Icons.notifications,
                  color: Colors.black,
                ),
                activeIcon: Icon(
                  Icons.notifications,
                  color: Colors.indigo,
                ),
                title: Text("Новости")),
          ],
        ),
      ),
    );
  }
}
